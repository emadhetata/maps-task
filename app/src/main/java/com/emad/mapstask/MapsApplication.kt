package com.emad.mapstask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MapsApplication: Application() {
}