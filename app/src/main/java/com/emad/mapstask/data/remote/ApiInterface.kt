package com.emad.mapstask.data.remote

import com.emad.mapstask.BuildConfig
import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.pojo.Place
import retrofit2.http.GET
import retrofit2.http.Query
import java.lang.reflect.Array

interface ApiInterface {
    @GET("/maps/api/place/autocomplete/json?")
    suspend fun getDestinations(@Query("input") input: String, @Query("key") key:String = BuildConfig.PLACES_KEY): Destination

    @GET("/maps/api/geocode/json?")
    suspend fun getLocationOfDestinations(@Query("place_id") place_id: String, @Query("key") key:String = BuildConfig.PLACES_KEY): Place

}