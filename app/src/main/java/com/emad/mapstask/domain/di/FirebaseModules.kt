package com.emad.mapstask.domain.di

import com.emad.mapstask.domain.repositories.HomeRepository
import com.emad.mapstask.domain.usecase.AllDriversUseCase
import com.emad.mapstask.domain.usecase.AllDriversUseCaseImpl
import com.emad.mapstask.domain.usecase.AllSourcesUseCase
import com.emad.mapstask.domain.usecase.AllSourcesUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object FirebaseModules {
    @Singleton
    @Provides
    fun provideSourcesUseCase(homeRepository: HomeRepository): AllSourcesUseCase =
        AllSourcesUseCaseImpl(homeRepository)


    @Singleton
    @Provides
    fun provideDriversUseCase(homeRepository: HomeRepository): AllDriversUseCase =
        AllDriversUseCaseImpl(homeRepository)

}