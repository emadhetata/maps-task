package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Destination

interface DestinationsUseCase {
    suspend operator fun invoke(input: String):Destination
}