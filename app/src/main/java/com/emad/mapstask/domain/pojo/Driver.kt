package com.emad.mapstask.domain.pojo

data class Driver(val driverName: String = "", val lat: Double = 0.0, val lng: Double = 0.0)
