package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.pojo.Place
import com.emad.mapstask.domain.repositories.HomeRepository
import javax.inject.Inject

class LocationOfDestinationUseCaseImpl @Inject constructor(private val homeRepository: HomeRepository): LocationOfDestinationUseCase{
    override suspend fun invoke(placeID: String) = homeRepository.getLocationOfDestination(placeID)
}