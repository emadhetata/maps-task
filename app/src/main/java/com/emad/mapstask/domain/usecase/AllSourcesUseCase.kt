package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Location

interface AllSourcesUseCase {

    suspend operator fun invoke(): ArrayList<Location>
}