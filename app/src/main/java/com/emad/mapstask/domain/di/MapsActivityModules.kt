package com.emad.mapstask.domain.di

import android.app.Activity
import com.emad.mapstask.domain.repositories.HomeRepository
import com.emad.mapstask.domain.usecase.*
import com.emad.mapstask.presentation.activities.MapsActivity
import com.emad.mapstask.presentation.adapters.LocationAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
@InstallIn(SingletonComponent::class)
@Module
object MapsActivityModules {


    @Singleton
    @Provides
    fun provideDestinationsUseCase(homeRepository: HomeRepository): DestinationsUseCase =
        DestinationsUseCaseImpl(homeRepository)

    @Singleton
    @Provides
    fun provideLocationOfDestinationsUseCase(homeRepository: HomeRepository): LocationOfDestinationUseCase =
        LocationOfDestinationUseCaseImpl(homeRepository)

}