package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.pojo.Place

interface LocationOfDestinationUseCase {
    suspend operator fun invoke(placeID: String): Place
}