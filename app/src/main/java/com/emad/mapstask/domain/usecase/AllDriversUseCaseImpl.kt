package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Driver
import com.emad.mapstask.domain.repositories.HomeRepository
import javax.inject.Inject

class AllDriversUseCaseImpl @Inject constructor(private val homeRepository: HomeRepository): AllDriversUseCase{
    override suspend fun invoke() = homeRepository.retrieveAllDrivers()
}