package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.repositories.HomeRepository
import javax.inject.Inject

class DestinationsUseCaseImpl @Inject constructor(private val homeRepository: HomeRepository): DestinationsUseCase{
    override suspend fun invoke(input: String) = homeRepository.getDestinations(input)

}