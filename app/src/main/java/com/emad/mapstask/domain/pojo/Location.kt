package com.emad.mapstask.domain.pojo

data class Location(var name: String = "", var latitude: Double = 0.0, var longitude: Double = 0.0)
