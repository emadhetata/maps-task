package com.emad.mapstask.domain.di

import android.app.Activity
import com.emad.mapstask.presentation.adapters.DestinationsAdapter
import com.emad.mapstask.presentation.adapters.LocationAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

object AdaptersModules {

    @Module
    @InstallIn(ActivityComponent::class)
    object LocationAdapterModule {
        @Provides
        fun provideClickListener(activity: Activity) =
            activity as LocationAdapter.ItemClickListener
    }

    @Module
    @InstallIn(ActivityComponent::class)
    object DestinationAdapterModule {
        @Provides
        fun provideClickListener(activity: Activity) =
            activity as DestinationsAdapter.ItemClickListener
    }
}