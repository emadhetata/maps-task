package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Location
import com.emad.mapstask.domain.repositories.HomeRepository
import javax.inject.Inject

class AllSourcesUseCaseImpl @Inject constructor(private val homeRepository: HomeRepository): AllSourcesUseCase {
    override suspend fun invoke(): ArrayList<Location> = homeRepository.retrieveAllSources()
}