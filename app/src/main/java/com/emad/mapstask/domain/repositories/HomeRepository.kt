package com.emad.mapstask.domain.repositories

import android.util.Log
import com.emad.mapstask.data.remote.ApiInterface
import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.pojo.Driver
import com.emad.mapstask.domain.pojo.Location
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class HomeRepository @Inject constructor(private val apiInterface: ApiInterface) {
    val db = FirebaseFirestore.getInstance()

    suspend fun retrieveAllSources(): ArrayList<Location> {
        val sourcesList = ArrayList<Location>()
        val sourcesCollection = db.collection("Sources")
        val allDocuments = sourcesCollection.get().await()
        for (document in allDocuments) {
            sourcesList.add(document.toObject(Location::class.java))
        }
        return sourcesList
    }

    suspend fun retrieveAllDrivers(): ArrayList<Driver> {
        val driversList = ArrayList<Driver>()
        val sourcesCollection = db.collection("Drivers")
        val allDocuments = sourcesCollection.get().await()
        for (document in allDocuments) {
            driversList.add(document.toObject(Driver::class.java))
        }
        return driversList
    }

    suspend fun getDestinations(input: String) = apiInterface.getDestinations(input)


    suspend fun getLocationOfDestination(placeID: String) = apiInterface.getLocationOfDestinations(placeID)


}