package com.emad.mapstask.domain.usecase

import com.emad.mapstask.domain.pojo.Driver

interface AllDriversUseCase {
    suspend operator fun invoke(): ArrayList<Driver>
}