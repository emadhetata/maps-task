package com.emad.mapstask.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.emad.mapstask.domain.pojo.Destination
import com.emad.mapstask.domain.pojo.Driver
import com.emad.mapstask.domain.pojo.Location
import com.emad.mapstask.domain.pojo.Place
import com.emad.mapstask.domain.usecase.AllDriversUseCase
import com.emad.mapstask.domain.usecase.AllSourcesUseCase
import com.emad.mapstask.domain.usecase.DestinationsUseCase
import com.emad.mapstask.domain.usecase.LocationOfDestinationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.util.ArrayList
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val allSourcesUseCase: AllSourcesUseCase,
    private val destinationsUseCase: DestinationsUseCase,
    private val locationOfDestinationUseCase: LocationOfDestinationUseCase,
    private val allDriversUseCase: AllDriversUseCase
) : ViewModel() {

    private val _allSourcesLiveData = MutableLiveData<ArrayList<Location>>()
    val allSourcesLiveData = _allSourcesLiveData

    private val _destinationLiveData = MutableLiveData<Destination>()
    val destinationLiveData = _destinationLiveData

    private val _locationOfDestination = MutableLiveData<Place>()
    val locationOfDestinationLiveData = _locationOfDestination

    private val _allDriversLiveData = MutableLiveData<ArrayList<Driver>>()
    val allDriversLiveData = _allDriversLiveData

    fun getAllSources() {
        viewModelScope.launch(IO) {
            _allSourcesLiveData.postValue(allSourcesUseCase.invoke())
        }
    }

    fun getDestinations(input: String) {
        viewModelScope.launch(IO) {
            _destinationLiveData.postValue(destinationsUseCase.invoke(input))
        }
    }

    fun getLocationOfDestinations(placeID: String) {
        viewModelScope.launch(IO) {
            _locationOfDestination.postValue(locationOfDestinationUseCase.invoke(placeID))
        }
    }

    fun getAllDrivers() {
        viewModelScope.launch(IO) {
            _allDriversLiveData.postValue(allDriversUseCase.invoke())
        }
    }
}