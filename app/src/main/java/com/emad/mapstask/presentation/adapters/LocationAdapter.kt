package com.emad.mapstask.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.emad.mapstask.domain.pojo.Location
import com.emad.mapstask.databinding.LocationHolderBinding
import javax.inject.Inject

class LocationAdapter @Inject constructor(private val itemClickListener: ItemClickListener) : RecyclerView.Adapter<LocationAdapter.MyViewHolder>() {
    val locations = ArrayList<Location>()

    inner class MyViewHolder(private val mBinding: LocationHolderBinding) :
        RecyclerView.ViewHolder(mBinding.root) {
        fun bind(location: Location){
            mBinding.locationName.text = location.name
            mBinding.locationName.setOnClickListener {
                itemClickListener.sourceItemClicked(location)
            }
        }
    }
    interface ItemClickListener {
        fun sourceItemClicked(location: Location)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LocationHolderBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(locations[position])
    }

    override fun getItemCount()= locations.size

    fun submitLocations(newLocation: ArrayList<Location>){
        locations.clear()
        locations.addAll(newLocation)
        notifyItemRangeInserted(0, newLocation.size-1)
    }
}