package com.emad.mapstask.presentation.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.Gravity
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.emad.mapstask.R
import com.emad.mapstask.databinding.ActivityMapsBinding
import com.emad.mapstask.domain.pojo.Driver
import com.emad.mapstask.presentation.adapters.DestinationsAdapter
import com.emad.mapstask.presentation.adapters.LocationAdapter
import com.emad.mapstask.presentation.viewmodel.HomeViewModel
import com.emad.mapstask.util.Constants.LOCATION_REQUEST_PERMISSION
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.emad.mapstask.domain.pojo.Location as MyLocation

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationAdapter.ItemClickListener,
    DestinationsAdapter.ItemClickListener {

    private var currentSelection = -1  //flag -> 0 for source  // 1 for distination
    private val TAG = "MapsActivity"
    private lateinit var mBinding: ActivityMapsBinding
    private lateinit var mMap: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener
    val homeViewModel by viewModels<HomeViewModel>()
    private lateinit var allSourcesList: ArrayList<MyLocation>
    @Inject lateinit var adapter: LocationAdapter
    @Inject lateinit var destinationAdapter: DestinationsAdapter
    var currentLocation: MyLocation? = null
    var destinationLocation: MyLocation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        initLocationManager()
        initLocationListener()
        checkLocationPermission()
        handleViews()
    }

    private fun initLocationManager() {
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
    }

    private fun initLocationListener() {
        locationListener = object : LocationListener {

            override fun onLocationChanged(location: Location) {
                moveCamera(location.latitude, location.longitude)
                locationManager.removeUpdates(locationListener)
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {
                askForGPS()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun connectLoctionListenerToLocationManager() {
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            0,
            0f,
            locationListener
        )

        locationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0,
            0f,
            locationListener
        )
    }

    private fun askForGPS() =
        startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            requestPermissions(
                this@MapsActivity, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), LOCATION_REQUEST_PERMISSION
            )
        } else {
            //  connectLoctionListenerToLocationManager()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_PERMISSION -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                ) {
                    connectLoctionListenerToLocationManager()
                } else {
                    Log.d(TAG, "onRequestPermissionsResult: permission denied")
                }
                return
            }
        }
    }

    private fun showLeftMenu() {
        mBinding.menuIcon.setOnClickListener {
            mBinding.layDrawer.setGravity(Gravity.LEFT)
            mBinding.drawerLayout.openDrawer(Gravity.LEFT)
        }
    }

    private fun handleViews() {
        mBinding.sourceLocationEditText.setOnClickListener {
            currentSelection = 0
            homeViewModel.getAllSources()
            homeViewModel.allSourcesLiveData.observe(this@MapsActivity, {
                allSourcesList = it
                initRecyclerView()
                adapter.submitLocations(it)
            })
        }
        mBinding.destinationLocationEditText.addTextChangedListener(destinationTextWatcher())
        showLeftMenu()
        getNearestDriver()
    }

    private fun getNearestDriver() {
        mBinding.requestRdButton.setOnClickListener {
            if (currentLocation != null) {
                homeViewModel.getAllDrivers()
                homeViewModel.allDriversLiveData.observe(this@MapsActivity, {
                    calculateTheNearestDriver(it)
                })
            }

        else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.select_ur_loc), Snackbar.LENGTH_LONG).show()
        }
    }

    private fun destinationTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                currentSelection = 1
                GlobalScope.launch(Main) {
                    delay(1000)
                    homeViewModel.getDestinations(s.toString())
                    homeViewModel.destinationLiveData.observe(this@MapsActivity, {
                        initRecyclerView()
                        destinationAdapter.submitDestination(it)
                    })
                }
            }

        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        mBinding.locationsRecyclerView.layoutManager = layoutManager

        if (currentSelection == 0)
            mBinding.locationsRecyclerView.adapter = adapter
        else if (currentSelection == 1)
            mBinding.locationsRecyclerView.adapter = destinationAdapter

        mBinding.locationsRecyclerView.visibility = VISIBLE
    }

    private fun calculateTheNearestDriver( driversList: ArrayList<Driver>) {
        lateinit var nearestLocation: MyLocation
        var minDes: Float = -1f
        val sourceLoc = Location(currentLocation!!.name)
        sourceLoc.latitude = currentLocation!!.latitude
        sourceLoc.longitude = currentLocation!!.longitude
        lateinit var destLoc: Location

        for (driver in driversList) {
            if (!driver.driverName.equals(sourceLoc.provider)) {
                destLoc = Location(driver.driverName)
                destLoc.latitude = driver.lat
                destLoc.longitude = driver.lng
                var distance = sourceLoc.distanceTo(destLoc)
                if (distance < minDes || minDes == -1f) {
                    minDes = distance
                    nearestLocation = MyLocation(driver.driverName, destLoc.latitude, destLoc.longitude)
                }
            }
        }
        Snackbar.make(findViewById(android.R.id.content), "Nearest Driver is ${nearestLocation.name}", Snackbar.LENGTH_LONG).show()
    }

    private fun moveCamera(latitude: Double = 38.7233174, longitude: Double = 35.6102733) {
        mMap.clear()
        val currentLocation = LatLng(latitude, longitude)
        mMap.addMarker(MarkerOptions().position(currentLocation).title("Hey !!"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        moveCamera() // default location Turkey
    }

    override fun sourceItemClicked(location: MyLocation) {
        currentLocation = location
        mBinding.sourceLocationEditText.setText("")
        mBinding.sourceLocationEditText.hint = location.name
        mBinding.locationsRecyclerView.visibility = GONE
    }

    override fun DestinationItemClicked(destinationName: String, placeID: String) {
        homeViewModel.getLocationOfDestinations(placeID)
        mBinding.destinationLocationEditText.setText("")
        mBinding.destinationLocationEditText.hint = destinationName
        mBinding.locationsRecyclerView.visibility = GONE

        homeViewModel.locationOfDestinationLiveData.observe(this@MapsActivity, {
            destinationLocation = MyLocation(
                destinationName,
                it.results[0].geometry.location.lat,
                it.results[0].geometry.location.lng
            )
            Log.d(TAG, "itemClicked: " + destinationLocation!!.latitude)
            Log.d(TAG, "itemClicked: " + destinationLocation!!.longitude)
        })
    }
}