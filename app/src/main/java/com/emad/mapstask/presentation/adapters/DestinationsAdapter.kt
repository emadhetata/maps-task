package com.emad.mapstask.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.emad.mapstask.databinding.LocationHolderBinding
import com.emad.mapstask.domain.pojo.Destination
import javax.inject.Inject

class DestinationsAdapter @Inject constructor(private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<DestinationsAdapter.MyViewHolder>() {

    private var currentDestination: Destination? = Destination()

    inner class MyViewHolder(private val mBinding: LocationHolderBinding) :
        RecyclerView.ViewHolder(mBinding.root) {
        fun bind(name: String, placeID: String) {
            mBinding.locationName.text = name
            mBinding.locationName.setOnClickListener {
                itemClickListener.DestinationItemClicked(name, placeID)
            }
        }
    }

    interface ItemClickListener {
        fun DestinationItemClicked(destinationName: String, placeID: String)
    }

    fun submitDestination( newDestination: Destination){
        currentDestination = null
        currentDestination = newDestination
        notifyItemRangeInserted(0, currentDestination!!.predictions.size-1)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LocationHolderBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(currentDestination!!.predictions[position].description, currentDestination!!.predictions[position].place_id)
    }

    override fun getItemCount() = currentDestination!!.predictions.size
}